call plug#begin()

Plug 'autozimu/LanguageClient-neovim', { 'do': ':UpdateRemotePlugins' }
Plug 'roxma/LanguageServer-php-neovim',  {'do': 'composer install && composer run-script parse-stubs'}

Plug 'SirVer/ultisnips'

Plug 'tpope/vim-surround'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'tomtom/tcomment_vim'
Plug 'alvan/vim-closetag'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'Raimondi/delimitMate'
Plug 'Shougo/deoplete.nvim',           { 'do': ':UpdateRemotePlugins' }
" Plug 'roxma/nvim-completion-manager'
Plug 'w0rp/ale' "linter

Plug 'morhetz/gruvbox'
Plug 'mhartington/oceanic-next'
" Plug 'vim-airline/vim-airline'
Plug 'trevordmiller/nova-vim'
Plug 'Shougo/vimfiler.vim'
Plug 'vimwiki/vimwiki'
Plug 'lervag/vimtex',                  { 'for': 'latex' }
Plug 'Shougo/unite.vim'
Plug 'qpkorr/vim-bufkill'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Plug 'LanguageTool'

Plug 'ap/vim-css-color',               { 'for': ['css', 'scss']}
Plug 'hail2u/vim-css3-syntax',         { 'for': ['css', 'scss']}
Plug 'othree/html5.vim'
"Plug 'pope/vim-haml'
"Plug 'jlanzarotta/bufexplorer'
" Plug 'vim-easy-align'
Plug 'godlygeek/tabular'


Plug 'zchee/deoplete-go',              { 'do': 'make'}
Plug 'fatih/vim-go'

Plug 'carlitux/deoplete-ternjs',       { 'for': ['javascript', 'javascript.jsx'] }
Plug 'ternjs/tern_for_vim',            { 'for': ['javascript', 'javascript.jsx'] }
Plug 'posva/vim-vue'
Plug 'heavenshell/vim-jsdoc',          { 'for': 'javascript'}

Plug 'racer-rust/vim-racer',           { 'for': 'rust' }
Plug 'rust-lang/rust.vim',             { 'for': 'rust' }

Plug 'ElmCast/elm-vim'
Plug 'pbogut/deoplete-elm'
call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Vim Behavior
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set number
set title
" set autoindent
" set autoindent noexpandtab tabstop=4 shiftwidth=4
set relativenumber
set hidden
set hlsearch                                               " highlight everything of the search
set clipboard+=unnamed,unnamedplus                         " use the system clipboard for yank/put/delete
"  set nobackup nowritebackup noswapfile autoread             " no backup or swap
set ignorecase                                             " Ignore case when searching
set ruler                                                  " show the cursor all the time                                      "
set linebreak                                              " break line only between word
set showbreak=>>                                           " indicate wraping text
set scrolloff=10                                           " scroll the window so we can always see 10 lines around the cursor
set inccommand=split
set showcmd                                                " show command in bottom bar
set cursorline                                             " highlight current line
set nocompatible
filetype indent on                                         " load filetype-specific indent files
set lazyredraw                                             " redraw only when we need to.
set wildmenu                                               " visual autocomplete for command menu
if (!has('nvim'))
  set encoding=utf-8 fileencoding=utf-8 termencoding=utf-8 " saving and encoding can only be set on startup in NeoVim
endif


set splitbelow
set splitright

highlight Folded guibg=grey guifg=blue
highlight FoldColumn guibg=blue guifg=white

set foldenable                                             " enable folding
set foldlevelstart=10                                      " open most folds by default
set foldnestmax=10                                         " 10 nested fold max
nnoremap <space> za
set foldmethod=indent                                      " fold based on indent level


" highlight last inserted text
nnoremap gV `[v`]


autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-= "disable autocoment on new lineo


:tnoremap <Esc> <C-\><C-n> "exit terminal mode



set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                  Appearance
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Or if you have Neovim >= 0.1.5
if (has("termguicolors"))
 set termguicolors
endif

" Theme
" syntax enable
" colorscheme OceanicNext
colorscheme gruvbox
" colorscheme nova
" colorscheme base16-solarized-dark
set background=dark
" set termguicolors
set colorcolumn=81

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Plugin Configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" UltiSnips
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsExpandTrigger="<C-j>"

" air-line
let g:airline_powerline_fonts = 1
set guifont=DejaVuSansCondensed\ for\ Powerine\ 10
let g:airline#extensions#tabline#enabled = 1

"vim-indent-guide
hi IndentGuidesOdd  ctermbg=black
hi IndentGuidesEven ctermbg=darkgrey
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_guide_size = 1
let g:indent_guides_color_change_percent = 20

" deoplete.
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_ignore_case = 1
let g:deoplete#auto_complete_start_length = 1
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
let g:deoplete#omni#functions = {}
let g:deoplete#omni#functions.javascript = [
  \ 'tern#Complete'
\]
set completeopt=longest,menuone,preview
let g:deoplete#sources = {}
let g:deoplete#sources['javascript.jsx'] = ['file', 'ultisnips', 'ternjs']
let g:tern#command = ['tern']
let g:tern#arguments = ['--persistent']
call deoplete#enable()

autocmd FileType javascript let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"


"tern
set completeopt-=preview "disable priview window"

" vimtex
let g:vimtex_enable = 1
let g:vimtex_view_method = 'zathura'

let g:languagetool_jar='/opt/LanguageTool-3.4/languagetool-commandline.jar'
let g:languagetool_lang='fr'


"racer
let g:racer_cmd =  "/home/hippo/.cargo/bin/racer"
" let $RUST_SRC_PATH= "~/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src"
let g:racer_experimental_completer = 1

"Language server
let g:LanguageClient_serverCommands = {
      \ 'rust': ['rustup', 'run', 'nightly', 'rls']
      \ }

let g:LanguageClient_autoStart = 1
autocmd FileType php LanguageClientStart


nnoremap <silent> K :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient_textDocument_rename()<CR>

"golang
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
" let g:go_auto_sameids = 1
let g:go_fmt_autosave = 0

"ale
let &runtimepath.=',~/.config/nvim/plugged/ale'
filetype plugin on

"vimFiler
:let g:vimfiler_as_default_explorer = 1
autocmd FileType vimfiler nmap <buffer> h <Plug>(vimfiler_switch_to_parent_directory)

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                   Mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let mapleader="\<SPACE>"
nnoremap <Leader>q :noh<CR> 
nnoremap <Leader>f gg=G

"enter a new line without changing mode
nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>

"duplicate line
noremap <c-d> yp
inoremap <c-d> <esc>yp
vnoremap <c-d> yp

"escape alternative
:imap jj <Esc>

"golang
au FileType go nmap <leader>rt <Plug>(go-run-tab)
au FileType go nmap <leader>rs <Plug>(go-run-split)
au FileType go nmap <leader>rv <Plug>(go-run-vertical)

nnoremap ; :

" move vertically by visual line
nnoremap j gj
nnoremap k gk

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                  Formatting
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set ts=2 sw=2 et 
filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Utils 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"Reload file when enter in file
au FocusGained,BufEnter * :silent! !
"Save file when leaving it
au FocusLost,WinLeave * :silent! w

"buffer management
" set wildchar=<Tab> wildmenu wildmode=full
" set wildcharm=<C-Z>
" " nnoremap <F10> :b <C-Z>
:nnoremap <Tab> :bnext<CR>
:nnoremap <S-Tab> :bprevious<CR>

"use the fzf-vim pluggin
nnoremap <F10> :Buffers <CR>
nnoremap <F9> :FZF <CR>
nnoremap <C-n> :VimFilerExplorer<CR>
let g:fzf_layout = { 'left': '~15%' }
" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1
